import Header from "./components/Header";
import Listing from "./components/Listing";
import Footer from "./components/Footer";
import "./Body.css";

export default function Body(){
    return( <body><Header /><Listing /><Footer /></body>)
}