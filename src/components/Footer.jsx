import face from "./image/face.png";
import insta from "./image/insta.png";
import twitter from "./image/twitter.png";
import phone from "./image/phone.png";
import "./Footer.css";



export default function Footer(){
    return( <footer><div className="info"><p>For more information visite <a href="#">Bateau-info</a></p></div>
    <div className="social"><a href=""><img src={face} alt="" /></a>
    <a href=""><img src={insta} alt="" /></a>
    <a href=""><img src={twitter} alt="" /></a>
    <a href=""><img src={phone} alt="" /></a>
    </div></footer> )
}