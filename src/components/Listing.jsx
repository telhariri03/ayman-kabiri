import im1 from "./image/bateau-1.png";
import im2 from "./image/bateau-2.jpeg";
import im3 from "./image/bateau-3.jpeg";
import im4 from "./image/bateau-4.jpg";
import im5 from "./image/bateau-5.jpeg";
import im6 from "./image/bateau-6.jpeg";
import "./Listing.css";


export default function Listing(){
    return( 
            <main>
            <div className="trtib">
                <div className="bateau"><img src={im1} alt="" /><a href=""><h3>bateau 1</h3></a></div>
                <div className="bateau"><img src={im2} alt="" /><a href=""><h3>bateau 2</h3></a></div>
                <div className="bateau"><img src={im3} alt="" /><a href=""><h3>bateau 3</h3></a></div> 
            </div>
            <div className="trtib">
                <div className="bateau"><img src={im4} alt="" /><a href=""><h3>bateau 4</h3></a></div>
                <div className="bateau"><img src={im5} alt="" /><a href=""><h3>bateau 5</h3></a></div>
                <div className="bateau"><img src={im6} alt="" /><a href=""><h3>bateau 6</h3></a></div> 
            </div>
            </main>)
}